package com.accenture.tcf.bars.dao;

/**
 * @author danielle.r.marasigan
 * @version 1.0 12 Sept 17
 */

import java.util.List;
import com.accenture.tcf.bars.domain.Record;

public interface IRecordDAO {

	public abstract List<Record> retrieveRecords();

}
