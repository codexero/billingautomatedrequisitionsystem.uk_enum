package com.accenture.tcf.bars.dao;

/**
 * @author danielle.r.marasigan
 * @version 1.0 12 Sept 17
 */

import com.accenture.tcf.bars.domain.Request;

public interface IRequestDAO {

	public abstract void insertRequest(Request request);

	public abstract void deleteRequest();

}
