package com.accenture.tcf.bars.dao;

/**
 * @version 1.0
 * @author maureen.kaye.c.sy
 *
 */


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.accenture.tcf.bars.datasource.MySQLDatasource;
import com.accenture.tcf.bars.domain.Record;


public class RecordDAOImpl implements IRecordDAO {

	private static final Logger LOGGER = Logger.getLogger(RecordDAOImpl.class);

	private static Connection conn;


	public RecordDAOImpl(Connection conn) {

	}


	public static Connection getConnection() {

		Connection conn = MySQLDatasource.getConnection();
		return conn;

	}


	public List<Record> retrieveRecords() {

		List<Record> recordList = new ArrayList<Record>();

	    try {
	    	PreparedStatement pstmt = conn.prepareStatement("SELECT billing.billing_cycle, billing.start_date, billing.end_date, customer.first_name, customer.last_name, billing.amount FROM  ((billing INNER JOIN account ON billing.account_id = account.account_id) INNER JOIN customer ON account.customer_id = customer.customer_id)");

	    	ResultSet rs = pstmt.executeQuery();

			while(rs.next())
			{
				int billingCycle = rs.getInt("billing_cycle");
		        Date startDate = rs.getDate("start_date");
		        Date endDate = rs.getDate("end_date");
		        String firstName = rs.getString("first_name");
		        String lastName = rs.getString("last_name");
		        double amount = rs.getDouble("amount");

		        Record record = new Record();
		        record.setBillingCycle(billingCycle);
		        record.setStartDate(startDate);
		        record.setEndDate(endDate);
		        record.setCustomerFirstName(firstName);
		        record.setCustomerLastName(lastName);
		        record.setAmount(amount);

				recordList.add(record);
			}

			conn.close();

		}catch (Exception e){
			LOGGER.info(e.getMessage());
		}

		return recordList;
	}


}




