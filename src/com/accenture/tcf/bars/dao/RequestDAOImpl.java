package com.accenture.tcf.bars.dao;

/**
 * @version 1.0
 * @author maureen.kaye.c.sy
 *
 */


import java.sql.*;
import org.apache.log4j.Logger;
import com.accenture.tcf.bars.datasource.MySQLDatasource;
import com.accenture.tcf.bars.domain.Request;


public class RequestDAOImpl implements IRequestDAO {


	private static final Logger LOGGER = Logger.getLogger(RequestDAOImpl.class);
	private static Connection conn;


	public RequestDAOImpl(Connection conn) {

	}


	public static Connection getConnection() {

		Connection conn = MySQLDatasource.getConnection();
		return conn;

	}


	@Override
	public void insertRequest(Request request) {

		try {

			PreparedStatement psmt = conn.prepareStatement("INSERT INTO request(billing_cycle, start_date, end_date) VALUES (?, ?, ?)");

			psmt.setInt(1, request.getBillingCycle());
			psmt.setDate(2, (Date) request.getStartDate());
			psmt.setDate(3, (Date) request.getEndDate());

			conn.setAutoCommit(false);

			int rows = psmt.executeUpdate();

			conn.commit();

			LOGGER.info(rows + " row(s) added!");

			conn.close();

		} catch (Exception e) {
			LOGGER.info(e.getMessage());
		}
	}


	@Override
	public void deleteRequest() {

		PreparedStatement pstmt;
			try {
				pstmt = conn.prepareStatement("DELETE FROM Request");
				int rows = pstmt.executeUpdate();
		    	LOGGER.info(rows + " row(s) deleted!");
			} catch (SQLException e) {
				e.printStackTrace();
			}

	    }


}


