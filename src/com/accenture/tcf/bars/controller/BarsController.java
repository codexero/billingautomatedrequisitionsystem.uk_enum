package com.accenture.tcf.bars.controller;

/**
 * @version 1.0
 * @author maureen.kaye.c.sy
 *
 */


import java.io.File;
import java.sql.Connection;
import java.util.List;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;

import com.accenture.tcf.bars.controller.BarsController;
import com.accenture.tcf.bars.dao.IRecordDAO;
import com.accenture.tcf.bars.dao.IRequestDAO;
import com.accenture.tcf.bars.datasource.MySQLDatasource;
import com.accenture.tcf.bars.domain.Record;
import com.accenture.tcf.bars.exception.BarsException;
import java.net.*;

@Controller
public class BarsController {

		@Autowired
		IRecordDAO recordDAOImpl;

		@Autowired
		IRequestDAO requestDAOImpl;

		//private static final Logger LOGGER = Logger.getLogger(BarsController.class);
		//private static Connection conn;

	private FileProcessor fileProcessor;
	private static Logger logger = Logger.getLogger(BarsController.class);


	public static Connection getConnection() {

		Connection conn = MySQLDatasource.getConnection();
		return conn;

	}


	@RequestMapping("/process.htm")
	public ModelAndView retrieveRecords(HttpServletRequest request, HttpServletResponse response) throws Exception {

		if (request.equals(null) || response.equals(null)) {
			logger.info("Request or Response failed for retrieveRecords Method.");
			throw new BarsException(BarsException.NO_RECORDS_TO_READ, new NullPointerException());
			
			
		}

		ModelAndView mv = new ModelAndView();
		File file = new File(request.getParameter("file"));

		fileProcessor = new FileProcessor();

		fileProcessor.execute(file);
		List<Record> recordList = fileProcessor.retrieveRecordfromDB();
		fileProcessor.writeOutput(recordList);

		mv.addObject("displayRecords", recordList);
		mv.setViewName("/success.jsp");
		return mv;

	}

}







