package com.accenture.tcf.bars.controller;

/**
 * @version 1.0
 * @author maureen.kaye.c.sy
 *
 */


import java.io.File;
import java.sql.Connection;
import java.util.List;

import com.accenture.tcf.bars.dao.IRecordDAO;
import com.accenture.tcf.bars.dao.IRequestDAO;
import com.accenture.tcf.bars.dao.RecordDAOImpl;
import com.accenture.tcf.bars.dao.RequestDAOImpl;
import com.accenture.tcf.bars.datasource.MySQLDatasource;
import com.accenture.tcf.bars.domain.Record;
import com.accenture.tcf.bars.domain.Request;
import com.accenture.tcf.bars.file.IInputFile;
import com.accenture.tcf.bars.file.IOutputFile;
import com.accenture.tcf.bars.file.XmlOutputFileImpl;
import com.accenture.tcf.bars.factory.InputFileFactory;


public class FileProcessor {

	private Connection conn;
	private IInputFile inputFile;
	private IOutputFile outputFile;
	private IRequestDAO requestDAO;
	private IRecordDAO recordDAO;


	public void execute(File file) {

		InputFileFactory inputFileFactory = InputFileFactory.getInstance();
		inputFile = inputFileFactory.getInputFile(file);
		inputFile.setFile(file);
		List<Request> requestList = inputFile.readFile();

		requestList.forEach(r -> {conn = MySQLDatasource.getConnection();
		requestDAO = new RequestDAOImpl(conn); requestDAO.insertRequest(r);});

//		for (Request r : requestList) {
//			requestDAO.insertRequest(r);
//		}

		outputFile = new XmlOutputFileImpl();
		outputFile.setFile(file);

	}


	public List<Record> retrieveRecordfromDB() {

		conn = MySQLDatasource.getConnection();
		recordDAO = new RecordDAOImpl(conn);
		List<Record> recordList = recordDAO.retrieveRecords();
		return recordList;

	}


	public void writeOutput(List<Record> recordList) {

		outputFile.writeFile(recordList);

	}


}




