package com.accenture.tcf.bars.test;




		import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.eclipse.jetty.util.log.Log;

import com.accenture.tcf.bars.exception.BarsException;

		public class BufferedReaderExample {

		    public static void main(String[] args) {

		        try (BufferedReader br = new BufferedReader(new FileReader("C:\\sample1.doc")))
		        {

		            String sCurrentLine;

		            while ((sCurrentLine = br.readLine()) != null) {
		                System.out.println(sCurrentLine);
		            }

		        } catch (IOException e) {
		        	//Log.info(BarsException.NO_SUPPORTED_FILE);
	             e.printStackTrace();
		        }


	}

}
