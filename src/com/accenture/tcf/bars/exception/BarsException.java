package com.accenture.tcf.bars.exception;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.hibernate.annotations.common.util.impl.Log;

/**
 * @version 1.0
 * @author elma.c.roxas
 *
 */


@SuppressWarnings("serial")
public class BarsException extends Exception {

	public static final String INVALID_START_DATE_FORMAT = "Invalid Start Date Format at row " ;
	public static final String INVALID_END_DATE_FORMAT = "ERROR: Invalid End Date Format at row " ;
	public static final String CYCLE_NOT_ON_RANGE = "Billing Cycle Not on Range at row ";
	public static final String PATH_DOES_NOT_EXIST = "Path does not Exist";
	public static final String 	NO_SUPPORTED_FILE = "File Format not Supported for Processing";
	public static final String NO_RECORDS_TO_READ = ("/error_no_records.html");
	public static final String NO_RECORDS_TO_WRITE = "No Record(s) To Write to the output file. ";

	private static Logger log = Logger.getLogger(BarsException.class);

	public BarsException(String message){
		Properties properties = new Properties();
		properties.setProperty("log4j.rootLogger", "DEBUG, CA");
		properties.setProperty("log4j.appender.CA", "org.apache.log4j.ConsoleAppender");
		properties.setProperty("log4j.appender.CA.layout", "org.apache.log4j.PatternLayout");
		properties.setProperty("log4j.appender.CA.layout.ConversionPattern", "%-4r %-5p [%c{1}] - %m%n");
		PropertyConfigurator.configure(properties);
		log.info(message);
		}

	public  BarsException(String message, Throwable cause){
		super(message,cause);
	}
}

