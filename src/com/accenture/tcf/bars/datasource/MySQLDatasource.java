package com.accenture.tcf.bars.datasource;

import java.sql.*;
//import com.mysql.jdbc.Connection;

public class MySQLDatasource {

	static Connection conn;

	public static Connection getConnection() {

		try {
			String url = "jdbc:mysql://localhost/bars_db";
			String user = "root";
			String pass = "abcd1234";

			//Loading the driver
			Class.forName("com.mysql.jdbc.Driver");

			//Obtaining a connection using DriverManager class
			conn = DriverManager.getConnection(url, user, pass);


		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return conn;
	}



}
