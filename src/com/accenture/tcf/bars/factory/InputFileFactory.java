package com.accenture.tcf.bars.factory;

import java.io.File;

import org.apache.commons.io.FilenameUtils;

import com.accenture.tcf.bars.file.CSVInputFileImpl;
import com.accenture.tcf.bars.file.IInputFile;
import com.accenture.tcf.bars.file.TextInputFileImpl;
/**
 * @version 1.0 
 * @author elma.c.roxas
 *
 */

public class InputFileFactory {

	private static InputFileFactory inputFileFactory;
	private InputFileFactory() {

	}

	public static InputFileFactory getInstance() {
		if (inputFileFactory == null) {
			inputFileFactory = new InputFileFactory();
		}
		return inputFileFactory;
	}

	public IInputFile getInputFile(File file) {
		String fileName = file.getName();

		if(FilenameUtils.isExtension(fileName, "txt")){
			return new TextInputFileImpl();
		} else if(FilenameUtils.isExtension(fileName, "csv")) {
			return new CSVInputFileImpl();
		} else {
			return null;
		}

	}
	
}
