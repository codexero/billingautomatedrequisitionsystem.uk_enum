package com.accenture.tcf.bars.domain;

/**
 * @author danielle.r.marasigan
 * @version 1.0 12 Sept 17
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.Date;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;

@Entity
@Table(name="Record")
public class Record {

	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column (name="billing_cycle")
	private int billingCycle;
	@Column (name="start_date")
	private Date startDate;
	@Column (name="end_date")
	private Date endDate;
	@Column (name="account_name")
	private String accountName;

	private String customerLastName;
	@Column (name="last_name")
	private String customerFirstName;
	@Column (name="amount")
	private double amount;

//	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinTable(name="account",joinColumns = { @JoinColumn(name = "account_id", referencedColumnName="account_id")}, inverseJoinColumns = { @JoinColumn(name = "customer_id", referencedColumnName="customer_id") })


	public int getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(int billingCycle) {
		this.billingCycle = billingCycle;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}
