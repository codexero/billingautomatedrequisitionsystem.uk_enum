package com.accenture.tcf.bars.domain;

/**
 * @author danielle.r.marasigan
 * @version 1.0 12 Sept 17
 */

import java.util.Date;
import java.util.Set;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;

import com.accenture.tcf.bars.domain.Record;

@Entity
@Table(name="Request")
public class Request {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="billingCycle")
	private int billingCycle;
	@Column(name="startDate")
	private Date startDate;
	@Column(name="endDate")
	private Date endDate;

//	@ManyToMany(cascade=CascadeType.ALL,mappedBy="registeredEvents", fetch = FetchType.EAGER)
//	private Set<Record> records;

//	public Set<Record> getRecord() {
//		return records;
//	}
//	public void setRecord(Set<Record> records) {
//		this.records = records;
//	}

	public int getBillingCycle() {
		return billingCycle;
	}
	public void setBillingCycle(int billingCycle) {
		this.billingCycle = billingCycle;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}


//	public Date setStartDate() {
//		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
//	    String s = dateFormat.format();
//		return startDate;
//	}


//	public Date getStartDate(ResultSet resultSet,Date columnName)
//	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
//	resultSet.getStartDate("startDate");
//
//	SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
//	String date =  new SimpleDateFormat("MM/dd/yyyy").format(columnName);
//	SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
//
//	String date = format1.format(columnName);
//
//	System.out.println(date);
//	// back to Date
//	Date date2 = format1.parse(date);
//	System.out.println(date2.toString());
//
//	Date date = new SimpleDateFormat("MM/dd/yyyy");
//
//	public static SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
//
//	Date converted = sdf.parse(dateString);

