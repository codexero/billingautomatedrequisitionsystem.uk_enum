package com.accenture.tcf.bars.file;

import java.io.BufferedReader;
 import java.io.File;
 import java.io.FileNotFoundException;
 import java.io.FileReader;
 import java.io.IOException;
 import java.sql.Date;
 import java.text.SimpleDateFormat;
 import java.time.LocalDate;
 import java.time.format.DateTimeFormatter;
 import java.util.ArrayList;
 import java.util.Arrays;
 import java.util.List;

import com.accenture.tcf.bars.domain.Request;
 import com.accenture.tcf.bars.exception.BarsException;
 import com.accenture.tcf.bars.file.AbstractInputFile;

public class CSVInputFileImpl extends AbstractInputFile {

 public List<Request> readFile() {
   Request newRequest;
   List<Request> request = new ArrayList<Request>();
//   File csvFile = getFile();
   BufferedReader br = null;
   String line = "";
   String csvSplitBy = ",";

  try {
//	  br = new BufferedReader(new FileReader(csvFile));
	 br = new BufferedReader(new FileReader("c://sample.csv"));
    while ((line = br.readLine()) != null) {
     String[] data = line.split(csvSplitBy);
     int billingCycle = Integer.parseInt(data[0]);
     String startDate = data[1];
     String endDate = data[2];
     String month;
     String day;
     String year;
     String[] split = line.split(csvSplitBy);

    try {
      if (billingCycle < 1 || billingCycle > 12) {
       throw new BarsException(
         BarsException.CYCLE_NOT_ON_RANGE,
         new Exception());
      } else if (!startDate.matches("([0-9]{2})\\/([0-9]{2})\\/([0-9]{4})")) {
       throw new BarsException(
         BarsException.INVALID_START_DATE_FORMAT,
         new Exception());
      }

     else if (!endDate.matches("([0-9]{2})\\/([0-9]{2})\\/([0-9]{4})")) {

      throw new BarsException(BarsException.INVALID_END_DATE_FORMAT,new Exception());
      }

     newRequest = new Request();
      newRequest.setBillingCycle(billingCycle);

     try {
       startDate = split[1].replace("/", "");
       month = startDate.substring(0, 2);
       day = startDate.substring(2, 4);
       year = startDate.substring(4, 8);
       Date sDate = Date.valueOf(year + "-" + month + "-" + day);
       newRequest.setStartDate(sDate);

      endDate = split[2].replace("/", "");
       month = endDate.substring(0, 2);
       day = endDate.substring(2, 4);
       year = endDate.substring(4, 8);
       Date eDate = Date.valueOf(year + "-" + month + "-" + day);
       newRequest.setEndDate(eDate);

     } catch (Exception e) {
       e.printStackTrace();
      }
      request.add(newRequest);

    } catch (Exception e) {
      e.printStackTrace();
     }
    }
   }

  catch (FileNotFoundException e) {
    e.printStackTrace();
   }

  catch (IOException e) {
    e.printStackTrace();
   }

  finally {
    if (br != null) {
     try {
      br.close();
     } catch (IOException e) {
      e.printStackTrace();
     }
    }

  }

  return request;
  }

 /*
   * package com.accenture.tcf.bars.file;
   *
   *
   * import java.io.BufferedReader;
   *
   * import java.io.File;
   *
   * import java.io.FileReader;
   *
   * import java.io.IOException;
   *
   * import java.sql.Date;
   *
   * import java.text.SimpleDateFormat;
   *
   * import java.time.LocalDate;
   *
   * import java.time.format.DateTimeFormatter;
   *
   * import java.util.ArrayList;
   *
   * import java.util.Arrays;
   *
   * import java.util.List;
   *
   *
   * import com.accenture.tcf.bars.domain.Request;
   *
   *
   * public class CSVInputFileImpl extends AbstractInputFile {
   *
   *
   * public List<Request> readFile() {
   *
   * List<Request> records = new ArrayList<Request>();
   *
   * String line;
   *
   * final String DELIMITER = ",";
   *
   * String startDate;
   *
   * String endDate;
   *
   * String month;
   *
   * String day;
   *
   * String year;
   *
   *
   * try {
   *
   * BufferedReader reader = new BufferedReader(new FileReader(
   *
   * "c://sample.csv"));
   *
   *
   * // new FileReader(getFile().getPath()));
   *
   *
   * while ((line = reader.readLine()) != null) {
   *
   * Request request = new Request();
   *
   * String[] split = line.split(DELIMITER);
   *
   *
   * request.setBillingCycle(Integer.parseInt(split[0]));
   *
   *
   * startDate = split[1].replace("/", "");
   *
   * month = startDate.substring(0, 2);
   *
   * day = startDate.substring(2, 4);
   *
   * year = startDate.substring(4, 8);
   *
   * Date sDate = Date.valueOf(year + "-" + month + "-" + day);
   *
   * request.setStartDate(sDate);
   *
   *
   * endDate = split[2].replace("/", "");
   *
   * month = endDate.substring(0, 2);
   *
   * day = endDate.substring(2, 4);
   *
   * year = endDate.substring(4, 8);
   *
   * Date eDate = Date.valueOf(year + "-" + month + "-" + day);
   *
   * request.setEndDate(eDate);
   *
   *
   * records.add(request);
   *
   * }
   *
   *
   * reader.close();
   *
   *
   * } catch (Exception e) {
   *
   * e.printStackTrace();
   *
   * return null;
   *
   * }
   *
   *
   * return records;
   *
   * }
   *
   *
   * public static void main(String[] args) throws IOException,
   *
   * ArrayIndexOutOfBoundsException {
   *
   *
   * try {
   *
   * CSVInputFileImpl text = new CSVInputFileImpl();
   *
   * List<Request> request = text.readFile();
   *
   * for (Request r : request) {
   *
   * System.out.println(r.getBillingCycle() + " " + r.getStartDate()
   *
   * + " " + r.getEndDate());
   *
   *
   * }
   *
   * // System.out.print(b);
   *
   *
   * } catch (Exception e) {
   *
   * e.printStackTrace();
   *
   * }
   *
   *
   * }
   *
   *
   * }
   */

 // try {
  // BufferedReader reader = new BufferedReader(new FileReader(
  // "c://sample.csv"));
  //
  // //new FileReader(getFile().getPath()));
  //
  // while ((line = reader.readLine()) != null) {
  // Request request = new Request();
  // String[] tokens = new String[3];
  // String[] DELIMITER = line.split(",");
  //
  // DELIMITER[1] = line.substring(2);
  // /*
  // tokens[0] = line.substring(0, 2);
  // DELIMITER[1] = line.split(tokens, 10);
  // tokens[1].replace("/", "");
  // tokens[2] = line.substring(10, 18);
  // tokens[2].replace("/", "");*/
  //
  // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  // DateTimeFormatter formatter =
  // DateTimeFormatter.ofPattern("MMM d yyyy");
  // LocalDate startDate = LocalDate.parse(tokens[1], formatter);
  // LocalDate endDate = LocalDate.parse(tokens[2], formatter);
  // /*java.util.Date startDate = sdf.parse(DELIMITER[1]);
  // java.util.Date endDate = sdf.parse(tokens[2]);*/
  // Date convertStartDate = new Date(startDate.getMonthValue());
  // //Date convertEndDate = new Date(endDate.getTime());
  //
  // request.setBillingCycle(Integer.parseInt(tokens[0]));
  // request.setStartDate(convertStartDate);
  // //request.setEndDate(convertEndDate);
  //
  // System.out.println(DELIMITER[1]);
  // System.out.println(startDate);
  // records.add(request);
  // }
  //
  // reader.close();
  //
  // } catch (Exception e) {
  // e.printStackTrace();
  // return null;
  // }
  //
  // return records;
  // }
  public static void main(String[] args) throws IOException,
    ArrayIndexOutOfBoundsException {

  CSVInputFileImpl csv = new CSVInputFileImpl();
   List<Request> bt = csv.readFile();

  for (Request e : bt) {
    System.out.print(e.getBillingCycle() + " " + e.getStartDate() + " "
      + e.getEndDate());
   }

  /*
    * try { CSVInputFileImpl text = new CSVInputFileImpl();
    * text.readFile(); //System.out.print(b); } catch (Exception e) {
    * e.printStackTrace(); }
    */

 }

@Override
public void setFile(File file) {
	// TODO Auto-generated method stub

}

@Override
public void getInputFile(File file) {
	// TODO Auto-generated method stub

}

}
