package com.accenture.tcf.bars.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.sql.Date;
import java.util.Calendar;

import com.accenture.tcf.bars.domain.Request;
import com.accenture.tcf.bars.exception.BarsException;
import com.accenture.tcf.bars.validation.IsValid;

//import org.apache.commons.lang3.math.NumberUtils;

public class TextInputFileImpl extends AbstractInputFile {

	public List<Request> readFile() {
		List<Request> records = new ArrayList<Request>();
		SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
		String line;
		Date startdateholder;
		Date enddateholder;
		int i = 0;
		File txtFile = getFile();

		try {
			BufferedReader br = new BufferedReader(new FileReader(txtFile));
//			BufferedReader reader = new BufferedReader(new FileReader("c://sample.txt"));

			while ((line = reader.readLine()) != null) {
				Request request = new Request();
				String[] str = new String[3];
				str[0] = line.substring(0, 2);
				str[1] = line.substring(2, 10); //start
				str[2] = line.substring(10, 18); //end

				if(!IsValid.isNumeric(str[0]) || (Integer.parseInt(str[0]) > 12)
						|| (Integer.parseInt(str[0]) < 1)) {
						throw new BarsException(BarsException.CYCLE_NOT_ON_RANGE + i,
								new Exception());
				} else if (!str[1].matches("([0-9]{8})")) {
					throw new BarsException(BarsException.INVALID_START_DATE_FORMAT + i,
							new Exception());
				} else if (!str[2].matches("([0-9]{8})")) {
					throw new BarsException(BarsException.INVALID_END_DATE_FORMAT + i,
							new Exception());
				}

				java.util.Date startDate = sdf.parse(str[1]);
				java.util.Date endDate = sdf.parse(str[2]);

				records.add(request);
//				System.out.println(Integer.parseInt(str[0]));
				System.out.println(startDate);
				System.out.println(endDate);
//				System.out.println(startdateholder);
//				System.out.println(enddateholder);
				// System.out.print(convertEndDate);
			}
			// reader.close();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return records;
	}

	public static void main(String[] args) throws IOException,
			ArrayIndexOutOfBoundsException {

		try {
			TextInputFileImpl text = new TextInputFileImpl();
			text.readFile();
			// System.out.print(b);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void setFile(File file) {
		// TODO Auto-generated method stub

	}

	@Override
	public void getInputFile(File file) {
		// TODO Auto-generated method stub

	}

}
