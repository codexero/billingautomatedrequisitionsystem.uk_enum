package com.accenture.tcf.bars.file;
/*
*
*@author
*angela.b.milallos
*version 1.0
*date: 8 September 2017
*
*/
import java.io.File;
import org.apache.log4j.Logger;

public abstract class AbstractOutputFile implements IOutputFile {
	private File file;
	protected static Logger logger;

	public File getFile() {
		return file;
	}


	public void setFile(File file) {
		this.file = file;
	}



}
