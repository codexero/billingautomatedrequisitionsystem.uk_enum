package com.accenture.tcf.bars.file;

import java.io.File;
import java.util.List;

import com.accenture.tcf.bars.domain.Request;
/**
 * @version 1.0
 * @author elma.c.roxas
 *
 */

public interface IInputFile {

	public abstract List<Request> readFile();

	public abstract void setFile(File file);

	public abstract void getInputFile(File file);

}

//import java.io.File;
//import java.util.List;
//import com.accenture.tcf.bars.domain.Request;
//
//public interface IInputFile {
//	public abstract List<Request> readFile();
//	public abstract void setFile (File file);
//	public abstract File getFile();
//
//}
