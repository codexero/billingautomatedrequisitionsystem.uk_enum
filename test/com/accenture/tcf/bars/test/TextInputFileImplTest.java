package com.accenture.tcf.bars.test;

import java.io.File;
import java.sql.Date;
import java.util.List;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.accenture.tcf.bars.domain.Request;
import com.accenture.tcf.bars.exception.BarsException;
import com.accenture.tcf.bars.factory.InputFileFactory;
import com.accenture.tcf.bars.file.IInputFile;
import com.accenture.tcf.bars.file.TextInputFileImpl;

/**
 *
 * Description:
 *
 * The TestInputFileImplTest class is used test the workflow conditions of the
 * TextInputFileImpl test class. It tests the normal condition and the exception
 * conditions for the class
 *
 *
 *
 *
 */
public class TextInputFileImplTest extends TestCase {

	TextInputFileImpl textinputfileimpl;

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	/**
	 *BARS.TC.010
	 */
	public void testReadTextValidRequestParameter() throws BarsException {
		File f = new File("TestFiles/TxtFiles/txtfiletest1.txt");
		InputFileFactory inputFF = InputFileFactory.getInstance();
		IInputFile in = inputFF.getInputFile(f);
		in.setFile(f);
		List<Request> req = in.readFile();

		for (Request r : req) {
			assertEquals(1, r.getBillingCycle());
			assertEquals(Date.valueOf("2013-01-01"), r.getStartDate());
			assertEquals(Date.valueOf("2013-01-31"), r.getEndDate());
		}
	}

	@Test
	/**
	 *BARS.TC.011
	 */
	public void testReadTextInvalidBillingCycle() throws BarsException {

		try {
			File f = new File("TestFiles/TxtFiles/txtfiletest2.txt");
			InputFileFactory inputFF = InputFileFactory.getInstance();
			IInputFile in = inputFF.getInputFile(f);
			in.setFile(f);


		} catch (Exception e) {
			assertEquals("ERROR: Billing Cycle not on range at row 1",
					e.getMessage());
		}

	}

	@Test
	/**
	 * BARS.TC.012
	 */
	public void testReadTextInvalidStartDateFormat() {
		try {
			File f = new File("TestFiles/TxtFiles/txtfiletest3.txt");
			InputFileFactory inputFF = InputFileFactory.getInstance();
			IInputFile in = inputFF.getInputFile(f);
			in.setFile(f);


		} catch (Exception e) {
			assertEquals("ERROR: Invalid Start Date format at row 1",
					e.getMessage());
		}
	}

	@Test
	/**
	 * BARS.TC.013
	 */
	public void testReadTextInvalidEndDateFormat() {
		try {
			File f = new File("TestFiles/TxtFiles/txtfiletest4.txt");
			InputFileFactory inputFF = InputFileFactory.getInstance();
			IInputFile in = inputFF.getInputFile(f);
			in.setFile(f);


		} catch (Exception e) {
			assertEquals("ERROR: Invalid End Date format at row 1",
					e.getMessage());
		}
	}

}
