package com.accenture.tcf.bars.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.accenture.tcf.bars.dao.IRecordDAO;
import com.accenture.tcf.bars.domain.Record;

@ContextConfiguration(locations = "classpath:/applicationContext.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@Component
public class RecordDAOImplTest extends TestCase {
	@Autowired
	private IRecordDAO recordDao;
	ArrayList<Record> retrieveRecords;

	@Before
	public void setUp() throws Exception {
		retrieveRecords = new ArrayList<Record>();
	}

	@After
	public void tearDown() throws Exception {
		retrieveRecords = null;

	}

	@Test
	public void testRetrieveRecords_Positive() {

		retrieveRecords = (ArrayList<Record>) recordDao.retrieveRecords();
		assertTrue(retrieveRecords.size() >= 1);
	}

	@Test
	public void testRetrieveRecords_Exception() {
		try {
			retrieveRecords = (ArrayList<Record>) recordDao.retrieveRecords();

		} catch (Exception e) {
			assertEquals("", e.getMessage());
		}

	}

}
