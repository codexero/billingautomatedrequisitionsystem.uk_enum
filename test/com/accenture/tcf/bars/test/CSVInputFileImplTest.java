package com.accenture.tcf.bars.test;
/*
*
*@author angela.b.milallos
*@version 1.0
*@date 12 September 2017
*
*
*/

import java.io.File;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import junit.framework.TestCase;
import com.accenture.tcf.bars.file.CSVInputFileImpl;


public class CSVInputFileImplTest extends TestCase {
	CSVInputFileImpl csvInputFileReader;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		csvInputFileReader = new CSVInputFileImpl();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
		csvInputFileReader = null;
	}

	@Test
	/**
	 * BARS.TS.005.01
	 *(Read Valid Request Parameter)
	 *
	 */
	public void testReadXmlValidRequestParameter() {
		//File csvFile = new File("TestFiles/CSV/CSV-valid.csv");
		File csvFile = new File("");
		csvInputFileReader.setFile(csvFile);
		assertTrue(csvInputFileReader.readFile().size() == 1);
	}


	@Test
	/**
	 * BARS.TS.006.01
	 * (Read Invalid Request With Invalid Billing Cycle)
	 *
	 */
	public void testReadXmlInvalidBillingCycle() {
		//File csvFile = new File("TestFiles/CSV/CSV-invalidBillingCycle.csv");
		File csvFile = new File("");
		csvInputFileReader.setFile(csvFile);
		assertTrue(csvInputFileReader.readFile().size() == 0);
	}


	@Test
	/**
	 * BARS.TS.006.02
	 *(Read Valid Request Parameter)
	 *
	 */
	public void testReadXmlInvalidStartDateFormat() {
		//File csvFile = new File("TestFiles/CSV/CSV-invalidStartDate.csv");
		File csvFile = new File("");
		csvInputFileReader.setFile(csvFile);
		assertTrue(csvInputFileReader.readFile().size() == 0);
	}


	@Test
	/**
	 * BARS.TS.006.03
	 * (Read Valid Request Parameter)
	 *
	 */
	public void testReadXmlInvalidEndDateFormat() {
		//File csvFile = new File("TestFiles/CSV/CSV-invalidEndDate.csv");
		File csvFile = new File("");
		csvInputFileReader.setFile(csvFile);
		assertTrue(csvInputFileReader.readFile().size() == 0);
	}
}
