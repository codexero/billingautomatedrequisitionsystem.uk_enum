//package com.accenture.tcf.bars.test;
//
//import java.io.File;
//
//import com.accenture.tcf.bars.factory.InputFileFactory;
//import com.accenture.tcf.bars.file.CSVInputFileImpl;
//import com.accenture.tcf.bars.file.TextInputFileImpl;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import junit.framework.TestCase;
///**
// * @version 1.0
// * @author elma.c.roxas
// *
// */
//
//
//public class InputFileFactoryTest extends TestCase {
//	InputFileFactory inputFileFactory;
//
//	@Before
//	public void setUp() throws Exception {
//		super.setUp();
//	}
//
//	@After
//	public void tearDown() throws Exception {
//		super.tearDown();
//	}
//
//	@Test
//	/**
//	 * BARS.TC.001.01: Instantiate an InputFileFactory
//	 *
//	 */
//	public void testGetInstance() {
//		inputFileFactory = InputFileFactory.getInstance();
//		assertTrue(inputFileFactory instanceof InputFileFactory);
//	}
//
//	@Test
//	/**
//	 *  BARS.TC.002.01: Get TextInputFileImpl instance.
//	 *
//	 */
//	public void testGetInputFileTxt() {
//		File file = new File("myTextFile.txt");
//		inputFileFactory = InputFileFactory.getInstance();
//		assertTrue(inputFileFactory.getInputFile(file) instanceof TextInputFileImpl);
//	}
//
//	@Test
//	/**
//	 *  BARS.TC.002.02: Get CSVInputFileImpl instance
//	 *
//	 */
//	public void testGetInputFileCsv() {
//		File file = new File("myCsvFile.csv");
//		inputFileFactory = InputFileFactory.getInstance();
//		assertTrue(inputFileFactory.getInputFile(file) instanceof CSVInputFileImpl);
//	}
//
//	@Test
//	/**
//	 *  BARS.TC.002.03: Get a null object
//	 *
//	 */
//	public void testGetNullInputFile() {
//		File file = new File("myWordFile.doc");
//		inputFileFactory = InputFileFactory.getInstance();
//		assertTrue(inputFileFactory.getInputFile(file) == null);
//	}
//
//
//}
